resource "aws_vpc" "kubernetes-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Kubernetes-VPC"
  }
}

resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.kubernetes-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public-net"
  }
}

resource "aws_subnet" "back-end-net" {
  vpc_id     = aws_vpc.kubernetes-vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "Private-net"
  }
}

resource "aws_internet_gateway" "kubernetes-gw" {
  vpc_id = aws_vpc.kubernetes-vpc.id

  tags = {
    Name = "Kubernetes-GW"
  }
}

resource "aws_route_table" "kubernetes-rt" {
  vpc_id = aws_vpc.kubernetes-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.kubernetes-gw.id
  } 

  tags = {
    Name = "Kubernetes-RT-Front"
  }
}

resource "aws_route_table_association" "kubernetes-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.kubernetes-rt.id
}

data "aws_ami" "ubuntu-latest" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical 
}

resource "aws_instance" "master-node" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t3.small"
  subnet_id                   = aws_subnet.front-end-net.id
  vpc_security_group_ids      = [aws_security_group.kubernetes-sg.id]
  associate_public_ip_address = true

  key_name = "kube"


  tags = {
    Name = "Master"
  }
}

resource "aws_instance" "worker-node-1" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  vpc_security_group_ids      = [aws_security_group.kubernetes-sg.id]
  associate_public_ip_address = true
  key_name = "kube"
  tags = {
    Name = "Worker-node-1"
  }
}

resource "aws_instance" "worker-node-2" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  vpc_security_group_ids      = [aws_security_group.kubernetes-sg.id]
  associate_public_ip_address = true
  key_name = "kube"
  tags = {
    Name = "Worker-node-2"
  }
}

resource "aws_instance" "ansible" {
  ami                         = data.aws_ami.ubuntu-latest.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  vpc_security_group_ids      = [aws_security_group.kubernetes-sg.id]
  associate_public_ip_address = true
  key_name = "kube"
  tags = {
    Name = "Ansible"
  }
}

output "master_node_public_ip" {
  value = aws_instance.master-node.public_ip
}

output "worker_node_1_public_ip" {
  value = aws_instance.worker-node-1.public_ip
}

output "worker_node_2_public_ip" {
  value = aws_instance.worker-node-2.public_ip
}